w = readdlm("data/wells.dat", comments=false)
f = open("data/wells.mads", "w")
for i = 1:size(w)[1]
	@printf f "- %s: { x: %.2f, y: %.2f, z0: %.2f, z1: %.2f, szindex: 1, obs: [" w[i,1] w[i,2] w[i,3] w[i,6]-w[i,4] w[i,6]-w[i,5]
	if typeof(w[i,7]) <: Number
		@printf f " 1: { t: 2016, c: %g } ] }\n" w[i,7]
	else
		c = float(split(w[i,7],"-"))
		@printf f "\n 1: { t: 2009, c: %g },\n" c[1]
		@printf f " 2: { t: 2016, c: %g } ] }\n" c[2]
	end
end
close(f)